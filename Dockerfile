FROM ubuntu:14.04
MAINTAINER Jon Brookes
ENV REFRESHED_AT 2015-05-17

RUN apt-get update
RUN apt-get install -y curl build-essential m4 libncurses5-dev libssh-dev 
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN apt-get update && apt-get install perl
RUN curl get.mojolicio.us | sh
RUN curl -L http://cpanmin.us | perl - Mojolicious::Plugin::AccessLog
ENV REREFRESHED_AT 2015-05-17
RUN mkdir /var/log/docker
ADD . /srv/www

EXPOSE 8080

WORKDIR /srv/www/my_app

CMD hypnotoad -f script/my_app
